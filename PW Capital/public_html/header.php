<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>PW Capitals - Strona Główna</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="PW Capitals - Strona Główna">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/slick.css" rel="stylesheet" type="text/css" />
        <link href="css/btn.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <main>
            <header>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="/" title="Przejdź do strony głównej">
                                <img src="img/logo.png" class="logo" alt="Logo PW Capitals">
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <!-- navigation -->
                            <ul class="navigation">
                                <li class="active">
                                    <a href="./o-nas.php" title="O nas">o nas</a>
                                </li>
                                <li>
                                    <a href="./gwarantowany-zysk.php" title="Gwarantowany zysk">gwarantowany zysk</a>
                                </li>
                                <li>
                                    <a href="./kontakt.php" title="Kontakt">kontakt</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>    
            </header>