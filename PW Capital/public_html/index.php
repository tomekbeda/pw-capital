<?php include("header.php"); ?>
<section id="slider-top">
    <div class="container-fluid">
        <div class="row">
            <div class="apllDown">
                <img src="img/top-slider-2.png" alt="PW Capital" />
            </div>
            <div class="line-topTwo"></div>
            <div class="short-description">
                <div class="line-top"></div>
                <h1>
                    Państwowe<br> Wierzytelności
                </h1>
                <h2>
                    bezpieczne lokowanie kapitału 
                </h2>
            </div>
        </div>
    </div>
</section> 
<section id="about_us">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="hexagon bg" skrollrToggle='{"distanceTop":200, "distanceBot":0}' >
                    <h2>
                        o nas
                    </h2>
                    <p>
                        Firma PW Capital z sukcesami działa <br>
                        na rynku wierzytelności państwowych <br>
                        od 2007 roku. Naszym głównym celem <br>
                        jest łączenie, w sposób bezpieczny i zgodny <br>
                        z prawem, inwestorów wraz z pracownikami<br>
                        spółek państwowych, chcących odsprzedać <br>
                        przysługujące im prawa do akcji firm, <br>
                        dla których pracują. 
                    </p>
                    <a href="" title="Dowiedz się więcej" class="read_more bg">
                        dowiedz się więcej
                    </a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="aplLeft bg" skrollrToggle='{"distanceTop":200, "distanceBot":0}'>
                    <img src="img/about-us-img.png" alt="PW Capital" skrollrToggle='{"distanceTop":400, "distanceBot":0}'/>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="threeBlock">
    <div class="container-fluid">
        <div class="row">
            <div class="aplBottom bg" skrollrToggle='{"distanceTop":200, "distanceBot":0}'>
                <img src="img/three-block-two.png" alt="PW Capital" skrollrToggle='{"distanceTop":400, "distanceBot":0}'/>
            </div>
            <div class="listBlock">
                <div class="block">
                    <div class="hexagon bg" skrollrToggle='{"distanceTop":300, "distanceBot":0}' >
                        <img src="img/ico-1.png" alt="PW Capital" class="ico"/>
                        <h2>
                            wygoda
                        </h2>
                        <p>
                            Do naszych klientów dojeżdżamy naterenie całej Polski.<br>
                            Gwarantujemy opiekę,w trakcie i po zakupie. Udzielamy <br>
                            pełnej pomocy przez cały okres trwania inwestycji. 
                        </p>
                    </div>
                </div>
                 <div class="block">
                       <div class="hexagon bg" skrollrToggle='{"distanceTop":350, "distanceBot":0}' >
                           <img src="img/ico-2.png" alt="PW Capital" class="ico"/>
                           <h2>
                               bezpieczeństwo
                           </h2>
                           <p>
                               Każda z przeprowadzanych transakcji poświadczana <br>
                               jest notarialnie, a pakiety akcji pochodzą wyłącznie <br>
                               od pracowników z najdłuższym stażem pracy. 
                           </p>
                       </div>
                   </div>
                   <div class="block">
                       <div class="hexagon bg" skrollrToggle='{"distanceTop":400, "distanceBot":0}' >
                           <img src="img/ico-3.png" alt="PW Capital" class="ico"/>
                           <h2>
                               zysk
                           </h2>
                           <p>
                               Proponowane przez nas produkty, tzw. państwowe <br>
                               wierzytelności generują czyste dochody z konkretnym <br>
                               zyskiem zagwarantowanym na dokumentach notarialnych. 
                           </p>
                           <a href="" title="Gwarantowany zysk" class="read_more bg">
                               gwarantowany zysk
                           </a>
                       </div>
                   </div>
            </div>
        </div>
    </div>
</section>
<?php include("footer.php"); ?>
