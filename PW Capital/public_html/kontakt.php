<?php include("header.php"); ?>
<section id="slider-top" class="bg-pages">
    <div class="container-fluid">
        <div class="row">
            <div class="apllDown">
                <div class="slider">
                    <img src="img/img-contact.png" class="pic" alt="PW Capital" />
                </div>
            </div>
            <div class="short-description">
                <h1>
                    kontakt
                </h1>
                <h2>
                    Masz pytanie?<br>Napisz do nas. Odpowiemy najszybciej<br>jak to tylko możliwe
                </h2>
            </div>
        </div>
    </div>
</section>
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <table skrollrToggle='{"distanceTop":300, "distanceBot":0}'>
                    <tr>
                        <td>
                            <img src="img/ico-4.png" alt="PW Capital">
                            <img src="img/ico-6.png"alt="PW Capital">
                        </td>
                        <td>
                            <p>
                                <b>P.W. Capital</b>
                            </p>
                            <p>
                                Ul. Bulw. Al. Totleben 2A<br>
                                Piętro 4
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-6">
                <table skrollrToggle='{"distanceTop":300, "distanceBot":0}'>
                    <tr>
                        <td>
                            <img src="img/ico-5.png" alt="PW Capital"/>
                            <img src="img/ico-7.png"alt="PW Capital"/>
                        </td>
                        <td>
                            <a href="mailto:biuro@pwcapital.com;" title="Napisz do nas"><b>biuro@pwcapital.com</b></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="aplBottom bg" skrollrToggle='{"distanceTop":200, "distanceBot":0}'>
                <img src="img/three-block-two.png" alt="PW Capital" skrollrToggle='{"distanceTop":400, "distanceBot":0}'/>
            </div>
            <form>
                <h2>
                    Formularz kontaktowy
                </h2>
                <div class="form-group field-contactdefault-name required has-error" skrollrToggle='{"distanceTop":200, "distanceBot":0}'>
                    <input type="text" id="contactdefault-name" class="form-control" name="ContactDefault[fname]" placeholder="Imię" aria-required="true" aria-invalid="true">
                    <p></p>
                </div>
                <div class="form-group field-contactdefault-name required has-error" skrollrToggle='{"distanceTop":200, "distanceBot":0}'>
                    <input type="text" id="contactdefault-email" class="form-control" name="ContactDefault[email]" placeholder="Email" aria-required="true" aria-invalid="true">
                    <p></p>
                </div>
                <div class="form-group field-contactdefault-name required has-error" skrollrToggle='{"distanceTop":200, "distanceBot":0}'>
                    <input type="text" id="contactdefault-subject" class="form-control" name="ContactDefault[subject]" placeholder="Temat" aria-required="true" aria-invalid="true">
                    <p></p>
                </div>
                <div class="form-group field-contactdefault-name required has-error" skrollrToggle='{"distanceTop":200, "distanceBot":0}'>
                    <textarea placeholder="Treść wiadomości"></textarea>
                    <p></p>
                </div>
                <div class="button btn btn-default draw">
                    <button id="send">
                        Wyślij wiadomosć
                    </button>
                </div>
                <div class="checkbox">
                    <label>
                        <div class="boxViews">
                            <img src="img/chcekbox-inside.png" alt="PW Capital">
                        </div>
                        <input type="checkbox">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi cursus, metus sit amet condimentum tempus, orci velit tincidunt eros, porta molestie ipsum urna ut diam. Morbi sed tristique erat. Maecenas sed mollis tortor. Donec eget gravida orci.
                    </label>
                </div>
            </form>
        </div>
    </div>
    <div class="right-widget" skrollrToggle='{"distanceTop":200, "distanceBot":0}'>
        <div class="right-widget-line" skrollrToggle='{"distanceTop":200, "distanceBot":0}'></div>
        <img src="img/right-widget-img.png" alt="PW Capital" skrollrToggle='{"distanceTop":200, "distanceBot":0}'/>
    </div>
</section>
<?php include("footer.php"); ?>