<div class="bottomLine" skrollrToggle='{"distanceTop":400, "distanceBot":0}'></div>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="space">
                <div class="text-top">
                    <h3>
                        Potrzebujesz<br>
                        więcej informacji ?<br>
                        Skontaktuj się z nami !
                    </h3>
                    <a href="" title="Przejdź do formularza kontaktowego" class="btnBottom">
                        formularz kontaktowy
                    </a>
                </div>
            </div>
            <div class="menuBottom">
                <div class="container">
                    <div class="line-footer" skrollrToggle='{"distanceTop":200, "distanceBot":0}'></div>
                    <div class="row">
                        <div class="col-sm-3">
                            <ul class="navBottom">
                                <li>
                                    <a href="" title="O nas">
                                        o nas
                                    </a>
                                </li>
                                <li>
                                    <a href="" title="Współpraca">
                                        współpraca
                                    </a>
                                </li>
                                <li>
                                    <a href="" title="Gwarantowany zysk">
                                        gwarantowany zysk
                                    </a>
                                </li>
                                <li>
                                    <a href="" title="Kontakt">
                                        kontakt
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <table>
                                <tr>
                                    <td>
                                        <b>P.W. Capital</b>
                                    </td>
                                    <td>
                                        Ul. Bulw. Al. Totleben 2A
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Bułgaria, Sofia 
                                    </td>
                                    <td>
                                        Piętro 4
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-3">
                           <!-- <a href="tel:+48736088195" title="Zadzwoń do nas"><b><img src="img/tel.png" alt="PW Capital"/>(+48) 736 088 195</b></a>-->
                            <a href="mailto:biuro@pwcapital.com;" title="Napisz do nas"><b><img src="img/mail.png" alt="PW Capital"/>biuro@pwcapital.com</b></a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="la">
                                <a href="" target="_blank" title="Odwiedź stronę oprogramowania">
                                    oprogramowanie: <b>Black Wolf CMS</b> <img src="img/wolf.png" alt=""/>
                                </a>
                                <a href="" target="_blank" title="Odwiedź stronę agencji">
                                    projekt i realizacja: <b>Laboratorium Artystyczne</b> <img src="img/la.png" alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</main>
<script type="text/javascript" src="js/cookies.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/counting.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>