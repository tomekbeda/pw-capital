jQuery(window).ready(function () {
    //animation top section when page load
    setTimeout(function () {
        $(".apllDown").addClass("load");
        setTimeout(function () {
            $(".apllDown img").addClass("load");
            setTimeout(function () {
                $(".logo").addClass("load");
                $(".line-topTwo").addClass("load");
                setTimeout(function () {
                    $(".short-description").addClass("load");
                    $("header ul.navigation").addClass("load");
                    setTimeout(function () {
                        $(".line-top").addClass("load");
                        setTimeout(function () {
                            $(".line-top").addClass("load2");
                        }, 2500);
                    }, 400);
                }, 100);
            }, 900);
        }, 400);
    }, 700);

    if(window.location.pathname.length > 1) {
       $("header").addClass("otherPage");
    } else {
       $("header").addClass("home");
    }

    //to animation header
    $(window).on('scroll', function (event) {
        var scrollValue = $(window).scrollTop();
        if (scrollValue > 120) {
            $('header').addClass('affix');
        } else {
            $('header').removeClass('affix');
        }
    });

    // scroll to animation element on page
    function skrollrToggle() {
        /* collect data */
        var pageH = window.innerHeight;
        var obj = document.querySelectorAll("[skrollrToggle]");
        var t, b, objTable, base, i;

        for (i = 0; i < obj.length; i++) {
            objTable = JSON.parse(obj[i].getAttribute("skrollrToggle"));
        }
        /* init data */
        for (i = 0; i < obj.length; i++) {

            /* distance calculate */
            base = obj[i].getBoundingClientRect().top - pageH;
            t = base + (objTable.distanceTop);
            b = base * (-1) - (objTable.distanceBot);

            /* run */
            ((t < 0) && (b < pageH) ? (obj[i].classList.add('skrollrToggle-run')) : "" /*(obj[i].classList.remove('skrollrToggle-run'))*/);
        }
    }

    if (document.querySelectorAll("[skrollrToggle]").length > 0) {
        window.addEventListener('scroll', function () {
            skrollrToggle();
        });
    }

    // library for the appearance of the sidebar
    if ($(window).width() > 767) {
        $(function () {
            $("body, .form-group textarea").niceScroll();
        });
    }
    ;

    // animation table on contact page
    if ($("#contact").length > 0)
        var offset = $("#contact").offset().top;
    wysokosc = $('#contact').outerHeight();
    wysokoscEkranu = screen.height;
    $(document).scroll(function () {
        if ($(window).scrollTop() >= offset - 500) {
            $("#contact table").each(function (index) {
                var el = $(this);
                el.css({
                    opacity: "1",
                    marginTop: "0"
                }, 400);
            });
        } else {
            $("#contact table").each(function (index) {
                var el = $(this);
                el.css({
                    opacity: "0",
                    marginTop: "70"
                }, 400);
            });
        }
    });
    if ($("#contact").length > 0)
        setTimeout(function(){ 
            console.log("hir")
        }, 3000);
        
        
    // Checbox in form on page contact
    $(".checkbox").click(function () {
        $(this).find(".boxViews").addClass("active");
    });


    if ($(window).width() > 767) {
        $(".number").counting();
    }
    
    $( ".movie" ).popupMovie({ });
    
  
    
    
    
});

